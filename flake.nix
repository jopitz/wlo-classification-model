{
  description = "The language model for wlo-classification";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    {
      # define an overlay to add wlo-classification-model to nixpkgs
      overlays.default = (final: prev: {
        inherit (self.packages.${final.system}) wlo-classification-model;
      });
    } //
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {inherit system;};
        nix-filter = self.inputs.nix-filter.lib;
        
        wlo-classification-model = pkgs.symlinkJoin {
          name = "wlo-classification-model-1.0.0";
          paths = [
            (nix-filter {
              root = self;
              include = [
                "variables"
                ./saved_model.pb
                ./keras_metadata.pb
              ];
            })
          ];
        };
      in
        {
          packages = rec {
            inherit wlo-classification-model;
            default = wlo-classification-model;
          };
        });
}
